//Variables
const cart              = document.querySelector('#carrito');
const cartListBody      = document.querySelector('#lista-carrito tbody');
const btnEmptyCart      = document.querySelector('#vaciar-carrito');
const coursesList       = document.querySelector('#lista-cursos');
let cartList = [];

//Functions
const getElementEvent = (e) => {
    return e.target;
}

const hasElement = (element) => {
    return element.firstChild;
}

const removeElement = (element) => {
    element.removeChild(element.firstChild);
}

const emptyCartList = () => {
    while( hasElement(cartListBody) ) {
        removeElement(cartListBody);
    }
}

const isCourseInCart = (courseInfo) => {
    let exists =  cartList.some((course) => course.id === courseInfo.id );
    return exists;
}

const updateCourse = (course) => {

    const courssUpdated = cartList.map((c) => {
        if (c.id === course.id ) {
            c.amount++;
            return c;
        } else {
            return c;
        }
    });

    return courssUpdated;
}

const show = () => {
    emptyCartList();

    cartList.forEach((course) => {
        const {image, title, price, amount, id} = course;

        const row = document.createElement('tr');
        row.innerHTML = `
            <td><img src="${image}" width="100"></td>
            <td>${title}</td>
            <td>${price}</td>
            <td>${amount}</td>
            <td><a href="#" class="borrar-curso" data-id="${id}">X</a></td>
        `;

        cartListBody.appendChild(row);
    })
}

const getCoursDetail= (course) => {
    const courseDetail = {
        image: course.querySelector('img').src,
        title: course.querySelector('h4').textContent,
        price: course.querySelector('.precio span').textContent,
        id: course.querySelector('a').getAttribute('data-id'),
        amount: 1
    }

    if ( ! isCourseInCart(courseDetail) ) {
        cartList = [...cartList, courseDetail];
    } else {
        const coursesUpdated = updateCourse(courseDetail);
        cartList = [...coursesUpdated];
    }

    show();  
}

const addCourse = (e) => {
    e.preventDefault();
    const element = getElementEvent(e) 
    if(element.classList.contains('agregar-carrito')) {
        const course = element.parentElement.parentElement;
        getCoursDetail(course)
    }
}

const deleteCart = (e) => {
    const element = getElementEvent(e);
    if (element.classList.contains('borrar-curso')) {
        const courseId = element.getAttribute('data-id');

        cartList = cartList.filter((course) => course.id !== courseId);

        show();
    }
}

//load event listeners
const loadEventListener = () => {
    coursesList.addEventListener('click', addCourse);

    cart.addEventListener('click', deleteCart);
}

loadEventListener();